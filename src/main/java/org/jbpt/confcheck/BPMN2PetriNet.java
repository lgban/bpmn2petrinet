package org.jbpt.confcheck;

import org.jbpt.petri.NetSystem;
import org.jbpt.petri.io.PNMLSerializer;
import org.jbpt.pm.ProcessModel;
import org.jbpt.pm.structure.ProcessModel2NetSystem;
import org.jbpt.utils.IOUtils;

import java.io.File;

public class BPMN2PetriNet {
    public static void main(String args[]) throws Exception {
        String model = "filtered_log_1"; // args[1];
        ProcessModel proc = BPMN2Reader.parse(new File("./models/" + model + ".bpmn"));
        IOUtils.toFile("output/" + model + ".pnml", PNMLSerializer.serializePetriNet(ProcessModel2NetSystem.transform(proc)));
    }
}
